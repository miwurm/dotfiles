# Just a few super-useful dotfiles.

Just create symlinks to the files - and that's it!

Have all your personal settings available on different machines! And never again waste time messing around with your environment setup.

Be efficient.

--------------

## How To Use

### Terminal
--------------
__.bashrc__

  Replace the original content of your .bashrc with:

  `source <your-path>/dotfiles/bash/bashrc`

__.inputrc__

  Create a symlink.

  `ln -s <your-path>/dotfiles/bash/inputrc ~/.inputrc`

### Git
--------------
__.gitconfig__

  Create a symlink.

  `ln -s <your-path>/dotfiles/git/gitconfig ~/.gitconfig`

__.gitignore_global__

  Create a symlink.

  `ln -s <your-path>/dotfiles/git/gitignore_global ~/.gitignore_global`

### Visual Studio Code
--------------
__keybindings.json__

  Create a symlink.

  `ln -s <your-path>/dotfiles/vscode/keybindings.json ~/.config/Code/User/keybindings.json`

__settings.json__

  Create a symlink.

  `ln -s <your-path>/dotfiles/vscode/settings.json ~/.config/Code/User/settings.json`

### htop
--------------
__htoprc__

  Create a symlink.

  `ln -s <your-path>/dotfiles/htop/htoprc ~/.config/htop/htoprc`

### Putty
--------------
__default_settings__

  Create a symlink.

  `ln -s <your-path>/dotfiles/putty/default_settings ~/.putty/sessions/Default%20Settings`


--------------
