###############################################################################
# This script installs several extensions.
# Run it once, whenever any new packages was installed.
# -----------------------------------------------------------------------------
#
# Whenever a new extension is added through the GUI, list it with
#     code --list-extensions
# and add it to the script below.
#
# -----------------------------------------------------------------------------
#
# Keep it simple.
# Save time.  :)
###############################################################################

code --install-extension austin.code-gnu-global
code --install-extension DavidAnson.vscode-markdownlint
code --install-extension eamodio.gitlens
code --install-extension James-Yu.latex-workshop
code --install-extension marus25.cortex-debug
code --install-extension ms-vscode.cpptools
code --install-extension ms-vscode.sublime-keybindings
code --install-extension mshr-h.veriloghdl
code --install-extension plorefice.devicetree
code --install-extension puorc.awesome-vhdl
code --install-extension rashwell.tcl
code --install-extension twxs.cmake
code --install-extension webfreak.debug
